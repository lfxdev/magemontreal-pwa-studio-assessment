# MageMontreal PWA Studio test

Requirements

Create a new project using the scaffolding tool and using the default Magento URL provided during the project initialization.

Add these customizations to the Venia storefront:

* Change the background color to **#F1E7E7** (Find the best way to do this)
* Create a new route (Using React Router) which should contain a list with all products using the Venia components (Gallery and Pagination)
  * The route should be: /see-all-products

# Best practices

* Please consider all the best practices of the PWA Studio documentation to create the customizations.
* Create a README file containing all the instructions on how we can configure and execute the application
